from os import listdir, chdir, getcwd, fspath, walk
from os.path import isfile, exists
# onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

#Make mkv videos and gifs out of all the png image sequences in a directory, recursively
# todo: give the ability to specify output format

def FolderSearch(topFolder):
    counter = 0
    directories = walk(topFolder)
    for (dirpath, dirnames, filenames) in directories:
        breakme = False
        if (len(filenames) > 0):
            for f in filenames:
                if (not ".png" in f):
                    breakme = True
                    break
        else:
            breakme = True
        if (breakme):
            continue
        # now we know it's just a .png sequence
        import subprocess
        print ("running ffmpeg in this directory... ", dirpath)
        chdir(dirpath)
        assert (getcwd() == dirpath)
        subprocess.call(["ffmpeg", "-r", "24", "-f", "image2", "-s", "1920x1080", "-i", "%04d.png", "-vcodec", "libx264", "-crf", "20",  "sequence" + "." + str(counter).zfill(3) + ".mkv"])
        subprocess.call(['ffmpeg', '-i', "sequence" + "." + str(counter).zfill(3) + ".mkv", '-vf', 'fps=24,scale=960:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse', '-loop', '0', "sequence" + "." + str(counter).zfill(3) + '.gif',])
        counter += 1
    return counter

#not needed anymore
# def filterFiles(f):
#     if (isfile(f)):
#         return False
#     if (not exists(f)):  
#         return False
#     return True

if __name__ == "__main__":
    import sys
    try:
        d = sys.argv[1]
    except IndexError:
        d = getcwd()
    print (d)
    print (FolderSearch(d))
