import bpy

#plan:
# iterate through scenes
# iterate through viewlayers
# iterate through cameras
#
# At this point, options:
#   Save a new .blend for each camera
#   use the script to render to file
# The second is the most straightforward, and possibly better
# Will go with the second

#To Do:
#   Creation of folders is done, and cameras are being selected correctly
#   Need to create render logic
#       Choose and set up render target
#           I won't bother with this yet, just gonna set it up myself each time
#           Now, that's only three times, so I'm OK
#       Set render directory
#       initiate the render
#   Once the render is complete, I think I'll write a Python script to use ffmpeg
#     to create a video for each image strip
#   Then I can just grab each .mp4 in the directory and I'm done :D



def RenderAll(self, context, wd, window):
    wd = wd + '.' + "render"
    rd = wd
    CreateFolder(location = wd)
    for scene in bpy.data.scenes:
        window.scene = scene # set active scene
        wd = rd + '/' + scene.name
        sd = wd
        CreateFolder(location=wd)
        for vl in scene.view_layers:
            wd = sd + '/' + vl.name
            vld = wd
            CreateFolder(location=wd)
            window.view_layer = vl
            # Create Folder for viewLayer
            cams = LayerCollectionSearch(
                vl.layer_collection,
                obType="CAMERA",
                nameKey="" )
            for cam in cams:
                scene.camera = cam
                bpy.ops.view3d.view_camera()
                for j in range(3):
                    if   (j == 0):
                        renderTarget = "WIREFRAME"
                    elif (j == 1):
                        renderTarget = "SHADED"
                    else:
                        renderTarget = "WIREFRAME_ON_SHADED"
                    wd = vld + '/' + cam.name + '/' + renderTarget
                    CreateFolder(location=wd)
                    Render(scene, wd + '/', context, renderTarget=renderTarget)
                bpy.ops.view3d.view_camera() #leave camera view

def LayerCollectionSearch(layer_collection, obType=None, nameKey=""):
    # a simple function for searching through a collection or nest of collections
    #  and finding a specific type of object, optionally by name
    # This will return a LIST of matching objects
    # eventually, I should support regex for name matching... but not today
    #
    foundObjects=set() #obs can be in multiple collections
    cur_lCol = layer_collection
    child_lCol = cur_lCol.children
    child_lCol = list(filter(filterExcludedCollection, child_lCol))
    canDescend = True if (len(child_lCol) > 0) else False
    search = ((canDescend) or (len(cur_lCol.collection.objects) > 0))
    prev_lCols = [layer_collection]
    skip_lCols = set() #
    while (search):
        while (canDescend == True): # could Walrus here instead in Python 3.8?
            prev_lCol = cur_lCol
            for lCol in child_lCol:
                if (lCol not in skip_lCols):
                    cur_lCol = lCol
                    child_lCol = cur_lCol.children
                    child_lCol = list(filter(filterExcludedCollection, child_lCol))
                    canDescend = True if (len(child_lCol) > 0) else False
                    if ((canDescend == True) and (cur_lCol not in skip_lCols)):
                        prev_lCols.append(cur_lCol)
                    break # only do one at a time
            if (cur_lCol == prev_lCol): 
                #we've checked all of them already
                canDescend = False
                child_lCol = []
        for lCol in child_lCol + [cur_lCol]:
            #search through each collection that is a leaf-level node, and the parent of the leaves
            if (lCol in skip_lCols): #might have already done this
                continue # collections can have multiple instances
            skip_lCols.add(lCol)
            for ob in lCol.collection.objects:
                if (searchTestObject(ob, obType)):
                    foundObjects.add(ob)
        if (prev_lCols):
            cur_lCol = prev_lCols.pop()
        else:
            search = False #we're done!
    return foundObjects

def filterExcludedCollection(lCol):
    if ((lCol.exclude == True) or
        (lCol.collection.hide_render == True)):
        return False
    return True

def searchTestObject(ob, obType=None, nameKey=""):
    print (ob.name)
    print (obType, ob.type)
    if (ob.type == obType):
        if (nameKey in ob.name):
            return True
        return False
    return False

def Render(scene, wd, context, renderTarget=None):
    if   (renderTarget == None):
        return
    for area in bpy.data.screens["Layout"].areas:
        if (area.type == "VIEW_3D"):
            space = area.spaces[0]
    if (renderTarget == "WIREFRAME"):
        space.overlay.show_wireframes = True
        space.shading.type = 'SOLID'
        space.shading.light = 'FLAT'
        space.shading.color_type = 'SINGLE'
        space.shading.single_color = (1, 1, 1)
    elif (renderTarget == "SHADED"):
        space.overlay.show_wireframes = False
        space.shading.type = 'RENDERED'
    elif (renderTarget == "WIREFRAME_ON_SHADED"):
        space.overlay.show_wireframes = True
        space.shading.type = 'RENDERED'
    scene.render.filepath = wd
    bpy.ops.render.opengl(
        animation=True,
        render_keyed_only=False,
        sequencer=False,
        write_still=False,
        view_context=True )
    
    #FAIL if either parameter is None
    # first, set up render
    # SAVE FILE to temp save, ideally recording progress somehow
    # for now, disable camera for rendering
    #   this doesn't do anything, but it's a useful flag for me to set
    return
def CreateFolder(location=""):
    #TODO: sanity check for location
    #TODO: find out how different operating systems handle this
    import os
    try:
        if (os.path.exists(location) == False):
            os.makedirs(location)
            print ("creating folder: ", location)
        else:
            print ("Folder already exists: ", location)
    except: #there's been some kind of failure
        return False
    return True

if __name__ == "__main__":
    RenderAll(None, bpy.context, '/home/theangerspecialist/Documents/Blender/STM/', bpy.context.window)


# print ("\n")
# screen = bpy.data.screens["Scripting"]
# for attr in dir(screen):
#     print (attr)
# print ("\n")
    

# print ("\n")
# for value in (screen.areas):
#     print (value.type)
#     if (value.type == "VIEW_3D"):
#         value.spaces[0].overlay.show_wireframes = False