bl_info = {
    "name": "iterateCameras",
    "author": "Node Spaghetti",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "F3 search",
    "description": "",
    "warning": "",
    "doc_url": "",
    "category": "Render",
}
import bpy
from . import iterateCameras


class IterateCameras(bpy.types.Operator):
    """Renders every camera in every viewlayer in every scene."""
    bl_idname = "object.iterate_cameras"
    bl_label = "iterateCameras"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        import time
        start = time.time
        window = context.window
        fp = bpy.data.filepath
        fp = fp.split(".blend")[0] #working directory
        iterateCameras.RenderAll(self, context, fp, window)
        self.report({"INFO"},("Operation completed in " + str(time.time-start)))
        return {'FINISHED'}


# Registration


def register():
    bpy.utils.register_class(IterateCameras)


def unregister():
    bpy.utils.unregister_class(IterateCameras)


if __name__ == "__main__":
    register()
