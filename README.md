# iterateCameras

A very simple addon for rendering every camera in a Blender scene to a separate
 file.

IMPORTANT: This script can *easily* produce tens of GB of data, depending on
 how you set it up. Be careful!

Right now, it's only useful for rendering wireframes, material previews, and
 wireframe-on-material-previews. It's not tested on Mac/Windows and might not
 work (since I used "/" characters instead of using os.join()... I was in a
 hurry). It's not tested on scenes with markers that change the scene's camera.
 
I'll extend this for more use-cases eventually.... but I only made this addon
 for myself, to quickly render out model show-cases.
 
Install the addon in the usual way -- zip it up and install in Blender, or copy
 the folder to your Blender's config folder (likely located in
 ~/.config/Blender/x.xx/scripts/addons).
 
DO NOT use this addon in an un-saved file. I don't know what it will do. It will
 probably just fail, since it asks Blender the location of the file, but who
 really knows? It might just fill your temp directory or something.

Use the addon by searching "iterate cameras" in the F3 search menu IN THE
 3D VIEWPORT (it won't work otherwise, since it uses the viewport to render).
 The viewport render settings are used for the material preview, not the
 rendered view. Also, the scene's render settings for e.g. resolution are
 respected. I reccomend using a low resolution until you've got good renders,
 since this script can produce lots of GBs of data... I made it to render
 60 cameras, and it produced 50GB with my final resolution of 200% 1080p.
 
The renders are placed in the same folder as your saved .blend file, in a
Renders
  >Scene
    >ViewLayer
      >Camera
        >wireframe
        >shaded
        >wireframe-on-shaded
 
I'm planning on making this a lot more useful, but if you get impatient
 waiting for me to do it... please fork and modify, or put in a pull request.
 
The utilities folder has a Python script for creating videos from .png's
 created by this script... of course you can also use the FFMPEG output
 directly within Blender.
 
Thanks, and happy Blending!

-Node Spaghetti

